package com.example.testmap

import android.graphics.PointF
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.longdo.api.IMapListener
import com.longdo.api.Line
import com.longdo.api.Map
import com.longdo.api.Pin
import com.longdo.api.type.MapLocation

class MainActivity : AppCompatActivity() {

    private lateinit var mMap: Map

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mMapGLSurfaceView = findViewById<com.longdo.api.MapGLSurfaceView>(R.id.mMapGLSurfaceView)
        mMapGLSurfaceView.setListener(IMapListener {
            initMap(it)
            initListener()
        })
    }

    private  fun initListener(){
        val btZoomIn = findViewById<MaterialButton>(R.id.btZoomIn)
        btZoomIn.setOnClickListener {
            mMap.zoomIn()
        }

        val btZoomOut = findViewById<MaterialButton>(R.id.btZoomOut)
        btZoomOut.setOnClickListener {
            mMap.zoomOut()
        }
    }

    private fun initMap(map: Map) {
        mMap = map

        val p1 = Pin(MapLocation(100.547562,
                13.743375), "https://findicons.com/files/icons/2796/metro_uinvert_dock/128/google_maps.png")
        val p2 = Pin(MapLocation(100.552315,
                13.727195), "https://findicons.com/files/icons/2796/metro_uinvert_dock/128/google_maps.png")

        p1.setOffset(PointF(0f, -0.5f))
        p2.setOffset(PointF(0f, -0.5f))

        mMap.pushPin(p1)
        mMap.pushPin(p2)

        val location = MapLocation(100.547562,
                13.743375)
        mMap.setLocation(location,
                false)

        val line = Line(arrMapLocation)
        line.setColor(255,
                0,
                0,
                255)
        line.width = 10

        mMap.addLine(line)
    }

    private val arrMapLocation = arrayOf<MapLocation>(
            MapLocation(100.547562,
                    13.743375),
            MapLocation(100.548035,
                    13.746095),
            MapLocation(100.548164,
                    13.746816),
            MapLocation(100.54821,
                    13.747088),
            MapLocation(100.548279,
                    13.747516),
            MapLocation(100.548561,
                    13.749132),
            MapLocation(100.548561,
                    13.749142),
            MapLocation(100.548592,
                    13.749369),
            MapLocation(100.548599,
                    13.749512),
            MapLocation(100.548584,
                    13.749699),
            MapLocation(100.548584,
                    13.749699),
            MapLocation(100.549423,
                    13.749776),
            MapLocation(100.549438,
                    13.749777),
            MapLocation(100.549461,
                    13.74978),
            MapLocation(100.549568,
                    13.749789),
            MapLocation(100.549713,
                    13.749803),
            MapLocation(100.549835,
                    13.749813),
            MapLocation(100.549835,
                    13.749813),
            MapLocation(100.549957,
                    13.748828),
            MapLocation(100.550018,
                    13.748328),
            MapLocation(100.550087,
                    13.747499),
            MapLocation(100.550072,
                    13.747404),
            MapLocation(100.550018,
                    13.747037),
            MapLocation(100.550056,
                    13.746675),
            MapLocation(100.550262,
                    13.74469),
            MapLocation(100.550301,
                    13.74438),
            MapLocation(100.550385,
                    13.743655),
            MapLocation(100.55043,
                    13.743323),
            MapLocation(100.550453,
                    13.743103),
            MapLocation(100.550491,
                    13.742865),
            MapLocation(100.550491,
                    13.742859),
            MapLocation(100.550491,
                    13.742855),
            MapLocation(100.550507,
                    13.742747),
            MapLocation(100.550514,
                    13.742723),
            MapLocation(100.550514,
                    13.742721),
            MapLocation(100.550659,
                    13.741709),
            MapLocation(100.550781,
                    13.741059),
            MapLocation(100.551064,
                    13.738489),
            MapLocation(100.55127,
                    13.73667),
            MapLocation(100.551422,
                    13.735119),
            MapLocation(100.551575,
                    13.733785),
            MapLocation(100.551682,
                    13.732834),
            MapLocation(100.551842,
                    13.73139),
            MapLocation(100.551987,
                    13.730105),
            MapLocation(100.552116,
                    13.728881),
            MapLocation(100.552185,
                    13.728319),
            MapLocation(100.552315,
                    13.727195),
    )

}